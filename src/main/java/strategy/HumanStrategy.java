package strategy;

import data.Point2D;

import java.util.List;

public class HumanStrategy implements Strategy {

	@Override
	public String getNextSysO(Point2D player, List<Point2D> humans, List<Point2D> zombies) {
		Point2D figure = humans.stream().findFirst().get();
		return figure.getX() + " " + figure.getY();
	}

}
