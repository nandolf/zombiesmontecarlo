package strategy;

import data.Point2D;

import java.util.List;

@FunctionalInterface
public interface Strategy {
	int speedPlayer = 1000;
	int speedZombie = 400;

	String getNextSysO(Point2D player, List<Point2D> humans, List<Point2D> zombies);
}
