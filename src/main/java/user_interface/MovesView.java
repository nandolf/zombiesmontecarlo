package user_interface;

import data.Point2D;

import javax.swing.*;
import java.util.Arrays;
import java.util.List;

public class MovesView extends JFrame {
    private JTextArea moveList = new JTextArea();

    MovesView() {
        setSize(400, 800);
        add(moveList);
        pack();
        setVisible(true);
        moveList.setVisible(true);
    }


    void setMoves(List<Point2D> moves) {
        moveList.setText("");
        moves.forEach(move -> moveList.append(move.toString() + "\n"));
    }

    public static void main(String[] args) {
        MovesView movesView = new MovesView();
        movesView.setMoves(Arrays.asList(new Point2D(2, 3),
                new Point2D(2, 3),
                new Point2D(2, 3),
                new Point2D(2, 3),
                new Point2D(2, 3),
                new Point2D(2, 3),
                new Point2D(2, 3),
                new Point2D(2, 3)));

    }
}
