package user_interface;

import data.Point2D;

import javax.swing.*;
import java.awt.*;
import java.util.Collections;
import java.util.List;

public class Spielfeld extends JPanel implements Printable {
    private Point2D player;
    private List<Point2D> humans;
    private List<Point2D> zombies;
    private long score = 0;

    private Graphics2D graphics;

    private static final long serialVersionUID = 1L;
    private static final int edgeSize = 1000;

    public Spielfeld() {
        this.player = new Point2D(-1000, -1000);
        this.humans = Collections.emptyList();
        this.zombies = Collections.emptyList();
        this.setBackground(Color.BLACK);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        this.graphics = (Graphics2D) g;
        graphics.scale(getWidth() / (16000.0 + 2 * edgeSize), getHeight() / (9000.0 + 2 * edgeSize));
        graphics.translate(edgeSize, edgeSize);
        graphics.setColor(Color.RED);
        graphics.drawRect(0, 0, 16000, 9000);
        humans.forEach(this::drawHuman);
        zombies.forEach(this::drawZombie);
        drawPlayer(player);
        drawScore(score);
    }

    private void drawScore(long score) {
        graphics.setFont(new Font("TimesRoman", Font.PLAIN, 300));
        graphics.setColor(Color.GREEN);
        graphics.drawString("Score: " + score, (int) (16000 * 0.75), -edgeSize / 2);
    }

    private void drawZombie(Point2D zombie) {
        drawKreis(zombie, Color.RED);
    }

    private void drawPlayer(Point2D player) {
        drawKreis(player, Color.BLUE);
        drawRangeCircle(player, Color.YELLOW);
    }

    private void drawRangeCircle(Point2D point, Color color) {
        int radius = 2000;
        graphics.setColor(color);
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics.drawOval((int) point.getX() - radius, (int) point.getY() - radius, radius * 2, radius * 2);
    }

    private void drawHuman(Point2D human) {
        drawKreis(human, Color.WHITE);
    }

    private void drawKreis(Point2D point, Color color) {
        int radius = 100;
        graphics.setColor(color);
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics.fillOval((int) point.getX() - radius, (int) point.getY() - radius, radius, radius);
    }

    @Override
    public void print(Point2D player, List<Point2D> humans, List<Point2D> zombies, long score) {
        this.player = player;
        this.humans = humans;
        this.zombies = zombies;
        this.score = score;
        this.repaint();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

}
