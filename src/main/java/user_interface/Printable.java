package user_interface;

import data.Point2D;

import java.util.List;

@FunctionalInterface
public interface Printable {
	void print(Point2D player, List<Point2D> humans, List<Point2D> zombies, long score);
}