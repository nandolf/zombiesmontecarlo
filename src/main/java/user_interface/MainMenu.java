package user_interface;

import data.DataProvider;
import data.Point2D;
import simulator.NullPrinter;
import simulator.Simulator;

import javax.swing.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MainMenu extends JFrame {
    private Spielfeld spielfeld;
    private JButton startSimulationButton;
    private JComboBox<LevelEntry> levelSelection;
    private JPanel mainPanel;

    List<LevelEntry> levels = new LinkedList<>();
    private Map<DataProvider, List<Point2D>> moves = new HashMap<>();
    private JButton startButton;
    private JButton saveButton;
    private JButton loadButton;

    public MainMenu() {
        setSize(1600, 900);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setContentPane(mainPanel);

        loadLevels();
        DataProvider.getAllSzenarios().forEach(level -> moves.put(level, new LinkedList<>()));

        startSimulationButton.addActionListener(e -> new Thread(this::startSimulation).start());
        printData(getCurrentSelectedLevel());
        levelSelection.addActionListener(e -> printData(getCurrentSelectedLevel()));

        JMenuBar menu = new JMenuBar();

        startButton = new JButton("Start");
        saveButton = new JButton("save current state");
        loadButton = new JButton("load last state");
        menu.add(startButton);
        menu.add(saveButton);
        menu.add(loadButton);

        setJMenuBar(menu);
        setVisible(true);
    }

    private DataProvider getCurrentSelectedLevel() {
        return ((LevelEntry) levelSelection.getSelectedItem()).getData();
    }

    public void setMoves(Map<DataProvider, List<Point2D>> moves) {
        this.moves = moves;
        updateLevels();
    }

    private void updateLevels() {
        for (LevelEntry level : levels) {
            Simulator sim = new Simulator(level.data, new NullPrinter());
            level.setScore(sim.simulateSteps(moves.get(level.data)));
        }
        levelSelection.repaint();
    }

    private void printData(DataProvider data) {
        spielfeld.print(data.getPlayer(), data.getHumans(), data.getZombies(), 0);
    }

    private void startSimulation() {
        DataProvider data = getCurrentSelectedLevel();
        printData(data);
        Simulator simulator = new Simulator(data, spielfeld);
        simulator.simulateSteps(moves.get(getCurrentSelectedLevel()));
    }

    private void loadLevels() {
        DataProvider.getAllSzenarios().forEach(level -> levels.add(new LevelEntry(level)));
        levels.forEach(levelSelection::addItem);
    }

    private void createUIComponents() {
        spielfeld = new Spielfeld();
    }

    class LevelEntry {
        private String score = "?";
        private DataProvider data;

        LevelEntry(DataProvider data) {
            this.data = data;
        }

        public DataProvider getData() {
            return data;
        }

        void setScore(long score) {
            this.score = String.valueOf(score);
        }

        @Override
        public String toString() {
            return data.getLevelName() + ": " + score;
        }
    }

    public void setStartAction(Runnable start) {
        startButton.addActionListener(e->new Thread(start).start());
    }

    public void setLoadAction(Runnable load) {
        loadButton.addActionListener(e->new Thread(load).start());
    }

    public void setSaveAction(Runnable save) {
        saveButton.addActionListener(e->new Thread(save).start());
    }



}
