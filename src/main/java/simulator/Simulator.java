package simulator;

import data.DataProvider;
import data.Point2D;
import strategy.Strategy;
import strategy.Util;
import user_interface.Printable;

import java.util.*;
import java.util.stream.Collectors;

public class Simulator {
    private List<Point2D> simulatedZombies;
    private List<Point2D> simulatedHumans;
    private Point2D simulatedPlayer;
    private Printable printer;

    private long currentScore = 0;


    public Simulator(DataProvider dataProvider) {
        this.simulatedZombies = dataProvider.getZombies().stream().map(Point2D::new).collect(Collectors.toList());
        this.simulatedHumans = dataProvider.getHumans().stream().map(Point2D::new).collect(Collectors.toList());
        this.simulatedPlayer = new Point2D(dataProvider.getPlayer());
        this.printer = new simulator.NullPrinter();
    }

    public Simulator(DataProvider dataProvider, Printable printer) {
        this.simulatedZombies = dataProvider.getZombies().stream().map(Point2D::new).collect(Collectors.toList());
        this.simulatedHumans = dataProvider.getHumans().stream().map(Point2D::new).collect(Collectors.toList());
        this.simulatedPlayer = new Point2D(dataProvider.getPlayer());
        this.printer = printer;
    }

    public Simulator(Simulator other) {
        this.simulatedZombies = other.simulatedZombies.stream().map(Point2D::new).collect(Collectors.toList());
        this.simulatedHumans = other.simulatedHumans.stream().map(Point2D::new).collect(Collectors.toList());
        this.simulatedPlayer = new Point2D(other.simulatedPlayer);
        this.printer = other.printer;
        this.currentScore = other.currentScore;
    }

    public void simulateOneStep(Point2D nextPosition) {
        try {
            simulateOneStepWithException(nextPosition);
        } catch (SimulatorException e) {
        }
    }

    private void simulateOneStepWithException(Point2D nextPosition) throws SimulatorException {
        try {
            zombiesWalkToTarget();
            ashMovesToTarget(nextPosition);
            currentScore += killZombies();
            eatHumans();
            printer.print(simulatedPlayer, simulatedHumans, simulatedZombies, currentScore);
        } catch (SimulatorException e) {
            printer.print(simulatedPlayer, simulatedHumans, simulatedZombies, 0);
            currentScore = 0;
            throw e;
        }

        if (simulatedHumans.isEmpty()) {
            currentScore = 0;
        }
    }

    public long simulateSteps(List<Point2D> moves) {
        for (Point2D move : moves) {
            try {
                simulateOneStepWithException(move);
            } catch (SimulatorException e) {
                return 0;
            }
        }

        if (simulatedHumans.isEmpty()) {
            return 0;
        }
        return currentScore;
    }

    public long simulate(Strategy strategy) {
        currentScore = 0;
        try {
            while (!simulatedZombies.isEmpty()) {
                simulateOneStepWithException(readNextPointFromStrategy(strategy));
                if (simulatedHumans.isEmpty()) {
                    printer.print(simulatedPlayer, simulatedHumans, simulatedZombies, 0);
                    currentScore = 0;
                    return currentScore;
                }
            }
        } catch (SimulatorException e) {
            return 0;
        }
        return currentScore;
    }

    private Point2D readNextPointFromStrategy(Strategy strategy) {
        String nextMove = strategy.getNextSysO(simulatedPlayer, simulatedHumans, simulatedZombies);
        String[] moves = nextMove.split(" ");
        return new Point2D(Double.parseDouble(moves[0]), Double.parseDouble(moves[1]));
    }

    private long killZombies() {
        long score = 0;
        long kombo = 1;
        Iterator<Point2D> iterator = simulatedZombies.iterator();
        while (iterator.hasNext()) {
            Point2D zombie = iterator.next();
            if (zombie.distance(simulatedPlayer) < 2000) {
                score += fib(++kombo) * simulatedHumans.size() * simulatedHumans.size() * 10;
                iterator.remove();
            }
        }
        return score;
    }

    private int fib(long kombo) {
        if (kombo == 0)
            return 0;
        if (kombo == 1)
            return 1;
        return fib(kombo - 1) + fib(kombo - 2);
    }

    private void zombiesWalkToTarget() throws simulator.SimulatorException {
        List<Point2D> allHumans = new ArrayList<>();
        allHumans.add(simulatedPlayer);
        allHumans.addAll(simulatedHumans);
        for (Point2D zombie : simulatedZombies) {
            Point2D targetHuman = Util.nearestFigure(zombie, allHumans);
            moveTowardsTarget(zombie, targetHuman, Strategy.speedZombie);
        }
    }

    private void ashMovesToTarget(Point2D nextPosition) throws simulator.SimulatorException {
        moveTowardsTarget(simulatedPlayer, nextPosition, Strategy.speedPlayer);
    }

    private void eatHumans() {
        for (Point2D zombie : simulatedZombies) {
            if (simulatedHumans.isEmpty()) {
                return;
            }
            Point2D human = Util.nearestFigure(zombie, simulatedHumans);
            if (zombie.distance(human) < 1) {
                simulatedHumans.remove(human);
            }
        }
    }

    private void moveTowardsTarget(Point2D player, Point2D target, int speed) throws simulator.SimulatorException {
        if (player.distance(target) < speed) {
            player.set(target);
            return;
        }
        Point2D walkDirection = target.subtract(player).normalize();
        Point2D nextPlayerPosition = player.add(walkDirection.multiply(speed));
        player.set(nextPlayerPosition);

        if (!Util.pointInField(nextPlayerPosition))
            throw new simulator.SimulatorException("Point out of field");
    }

    public Point2D getPositionPlayer() {
        return new Point2D(simulatedPlayer);
    }

    public boolean isFinished() {
        return simulatedZombies.isEmpty() || simulatedHumans.isEmpty();
    }

    public long getCurrentScore() {
        return currentScore;
    }

    public long highestPossibleScore() {
        long score = 0;
        long kombo = 1;
        Set<Point2D> zombiesCopy = new HashSet<>(simulatedZombies);
        Iterator<Point2D> iterator = zombiesCopy.iterator();
        while (iterator.hasNext()) {
            iterator.next();
            score += fib(++kombo) * simulatedHumans.size() * simulatedHumans.size() * 10;
            iterator.remove();
        }
        return score;
    }
}
