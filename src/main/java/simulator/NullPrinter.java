package simulator;
import data.Point2D;
import user_interface.Printable;

import java.util.List;

public class NullPrinter implements Printable{

	@Override
	public void print(Point2D player, List<Point2D> humans, List<Point2D> zombies, long score) {
	}

}
