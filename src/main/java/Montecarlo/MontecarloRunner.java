package Montecarlo;

import data.DataProvider;
import data.Point2D;
import simulator.Simulator;
import strategy.Util;
import user_interface.MainMenu;

import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

public class MontecarloRunner {
    private final MainMenu mainMenu;
    private Map<DataProvider, List<Point2D>> movesPerLevel = new ConcurrentHashMap<>();
    private Map<DataProvider, Long> scoreMaxPerLevel = new ConcurrentHashMap<>();

    private static final double WEIGHT_RING_0 = 0.1;
    private static final double WEIGHT_RING_1 = 0.2;
    private static final double WEIGHT_RING_2 = 0.3;
    private static final double WEIGHT_RING_3 = 0.4;

    private static final Long WANTED_SCORE = 2000000L;

    MontecarloRunner(MainMenu mainMenu) {
        this.mainMenu = mainMenu;
        for (DataProvider dataProvider : DataProvider.getAllSzenarios()) {
            movesPerLevel.put(dataProvider, new LinkedList<>());
            scoreMaxPerLevel.put(dataProvider, 0L);
        }
    }

    void run()  {
        long lastOverallscore = 0;
        while (!finalScoreReached()) {
            DataProvider.getAllSzenarios().parallelStream().forEach(dataProvider -> {
                Simulator simulator = new Simulator(dataProvider);
                List<Point2D> moves = new LinkedList<>();
                List<Point2D> currentBestStrategy = new LinkedList<>(movesPerLevel.get(dataProvider));
                while (!simulator.isFinished()) {
                    currentBestStrategy = findBestStrategy(simulator, currentBestStrategy, 1000);
                    Point2D nextPosition;
                    if (currentBestStrategy.isEmpty()) {
                        nextPosition = getNextRandomPosition(simulator.getPositionPlayer());
                    } else {
                        nextPosition = currentBestStrategy.remove(0);
                    }
                    simulator.simulateOneStep(nextPosition);
                    moves.add(nextPosition);
                    if (scoreCannotBeBest(dataProvider, simulator) && simulator.getCurrentScore() < scoreMaxPerLevel.get(dataProvider)) {
                        break;
                    }

                }
                System.out.println(dataProvider.getLevelName() + " finished. Overallscore:  " + currentOverallScore());
                if (simulator.getCurrentScore() > scoreMaxPerLevel.get(dataProvider)) {
                    scoreMaxPerLevel.put(dataProvider, simulator.getCurrentScore());
                    movesPerLevel.put(dataProvider, moves);
                    mainMenu.setMoves(movesPerLevel);
                }
            });
            if (currentOverallScore() > lastOverallscore) {
                System.out.println(currentOverallScore());
                lastOverallscore = currentOverallScore();
            }
        }

    }


    private List<Point2D> findBestStrategy(Simulator simulator, List<Point2D> currentBestStrategy, int timeOut) {
        List<Point2D> bestMoves = currentBestStrategy;
        Simulator currentSimulator = new Simulator(simulator);
        currentSimulator.simulateSteps(currentBestStrategy);
        long bestScore = currentSimulator.getCurrentScore();
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < timeOut) {
            currentSimulator = new Simulator(simulator);
            List<Point2D> currentMoves = new LinkedList<>();
            while (!currentSimulator.isFinished() && (System.currentTimeMillis() - start < timeOut)) {
                Point2D nextRandomPosition = getNextRandomPosition(simulator.getPositionPlayer());
                currentSimulator.simulateOneStep(nextRandomPosition);
                currentMoves.add(nextRandomPosition);
            }
            if (currentSimulator.getCurrentScore() > bestScore) {
                bestMoves = currentMoves;
                bestScore = currentSimulator.getCurrentScore();
            }
        }
        return bestMoves;
    }

    private boolean scoreCannotBeBest(DataProvider dataProvider, Simulator simulator) {
        return simulator.highestPossibleScore() <= scoreMaxPerLevel.get(dataProvider);
    }

    private boolean finalScoreReached() {
        return currentOverallScore() >= WANTED_SCORE;
    }

    private long currentOverallScore() {
        return scoreMaxPerLevel.values().stream().mapToLong(Long::longValue).sum();
    }

    Point2D getNextRandomPosition(Point2D positionPlayer) {
        Point2D nextPosition;
        do {
            int ring = getNextRing();
            int positionOnRing = ThreadLocalRandom.current().nextInt(500);
            int angle = ThreadLocalRandom.current().nextInt(360);
            nextPosition = getNextPosition(ring, angle, positionOnRing, positionPlayer);
        } while (!Util.pointInField(nextPosition));
        return nextPosition;
    }

    private int getNextRing() {
        List<Double> weights = Arrays.asList(WEIGHT_RING_0, WEIGHT_RING_1, WEIGHT_RING_2, WEIGHT_RING_3);
        double random = Math.random() * (WEIGHT_RING_0 + WEIGHT_RING_1 + WEIGHT_RING_2 + WEIGHT_RING_3);
        for (int i = 0; i < 4; i++) {
            random -= weights.get(i);
            if (random <= 0.0) {
                return i;
            }
        }
        return 3;
    }

    private Point2D getNextPosition(int ring, int angle, int positionOnRing, Point2D positionPlayer) {
        int length = ring * 500 + positionOnRing;
        Point2D up = new Point2D(0, length);
        Point2D rotatedUp = Util.rotateVector(up, angle);
        return positionPlayer.add(rotatedUp);
    }


    void save() {
        try {
            ObjectOutputStream out;
            try (FileOutputStream fileOut = new FileOutputStream("stategies.save")) {
                out = new ObjectOutputStream(fileOut);
                out.writeObject(movesPerLevel);
                out.writeObject(scoreMaxPerLevel);
                System.out.println("saved");
            }

        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    void load() {
        try {
            try (FileInputStream fileIn = new FileInputStream("stategies.save")) {
                ObjectInputStream in = new ObjectInputStream(fileIn);
                movesPerLevel = (ConcurrentHashMap<DataProvider, List<Point2D>>) in.readObject();
                scoreMaxPerLevel = (ConcurrentHashMap<DataProvider, Long>) in.readObject();
                in.close();
                mainMenu.setMoves(movesPerLevel);
                System.out.println("loaded");
            }
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("last state not found");
            c.printStackTrace();
        }
    }

    Map<DataProvider, List<Point2D>> getMovesPerLevel() {
        return movesPerLevel;
    }

    Map<DataProvider, Long> getScoreMaxPerLevel() {
        return scoreMaxPerLevel;
    }
}
