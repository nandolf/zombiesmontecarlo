package Montecarlo;

import simulator.SimulatorException;
import user_interface.MainMenu;

public class Main {

    public static void main(String[] args) throws SimulatorException {
        MainMenu mainMenu = new MainMenu();
        MontecarloRunner montecarloRunner = new MontecarloRunner(mainMenu);
        mainMenu.setStartAction(montecarloRunner::run);
        mainMenu.setLoadAction(montecarloRunner::load);
        mainMenu.setSaveAction(montecarloRunner::save);
    }


}
