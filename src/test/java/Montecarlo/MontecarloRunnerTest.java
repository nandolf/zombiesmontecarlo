package Montecarlo;

import data.DataProvider;
import data.Point2D;
import org.junit.Test;
import org.mockito.Mockito;
import user_interface.MainMenu;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class MontecarloRunnerTest {

    @Test
    public void testDistribution() throws Exception {
        BufferedImage bi = new BufferedImage(4000, 4000, BufferedImage.TYPE_INT_ARGB);
        Graphics2D ig2 = bi.createGraphics();
        MontecarloRunner mo = new MontecarloRunner(Mockito.mock(MainMenu.class));
        for (int i = 0; i < 100000; i++) {
            Point2D randomPosition = mo.getNextRandomPosition(new Point2D(2000, 2000));
            ig2.drawRect((int) randomPosition.getX(), (int) randomPosition.getY(), 10, 10);
        }
        ImageIO.write(bi, "PNG", new File("yourImageName.PNG"));
    }


    @Test
    public void testSaveAndLoad() throws Exception {
        MontecarloRunner montecarloRunner = new MontecarloRunner(Mockito.mock(MainMenu.class));
        montecarloRunner.getMovesPerLevel().put(DataProvider.hoard(), Collections.singletonList(new Point2D(42, 42)));
        montecarloRunner.getScoreMaxPerLevel().put(DataProvider.hoard(), 100L);
        montecarloRunner.save();

        MontecarloRunner monteLoad = new MontecarloRunner(Mockito.mock(MainMenu.class));
        monteLoad.load();
        assertThat(monteLoad.getMovesPerLevel().get(DataProvider.hoard()), is(Collections.singletonList(new Point2D(42, 42))));
        assertThat(monteLoad.getScoreMaxPerLevel().get(DataProvider.hoard()), is(100L));
    }
}