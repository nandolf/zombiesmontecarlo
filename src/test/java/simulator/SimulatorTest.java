package simulator;

import data.DataProvider;
import data.Point2D;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import strategy.HumanStrategy;
import strategy.Strategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertThat;

public class SimulatorTest {

    private Strategy strategy = new HumanStrategy();

    class ZombieStrategy implements Strategy {

        @Override
        public String getNextSysO(Point2D player, List<Point2D> humans, List<Point2D> zombies) {
            Point2D target = zombies.stream().findFirst().get();
            return target.getX() + " " + target.getY();
        }
    }

    @Before
    public void before() {
        strategy = new HumanStrategy();
    }

    @Test
    public void testScoreSingle() {
        long score = testScore(player(0, 0), list(human(0, 0)), list(zombie(0, 2000)));
        assertThat(score, is(10));
    }

    @Test
    public void testScoreKombo() {
        long score = testScore(player(0, 0), list(human(0, 0)), list(zombie(0, 2000), zombie(2000, 0)));
        assertThat(score, is(10 + 20));
    }

    @Test
    public void testScoreMultiple() {
        long score = testScore(player(0, 0), list(human(0, 0), human(4000, 4000)), list(zombie(0, 2000)));
        assertThat(score, is(2 * 2 * 10));
    }

    @Test
    public void testScoreMultipleKombo() {
        long score = testScore(player(0, 0), list(human(0, 0), human(4000, 4000)),
                list(zombie(0, 2000), zombie(2000, 0)));
        assertThat(score, is(2 * 2 * 10 + 2 * 2 * 20));
    }

    @Test
    public void testNoHope() throws Exception {
        // the second zombie will found no human
        long score = testScore(player(0, 0), list(human(4000, 4000)), list(zombie(4000, 4000), zombie(4001, 4000)));
        assertThat(score, is(0));
    }

    @Test
    public void testPlayerOutOfField() throws Exception {
        long score = testScore(player(-10000, 0), list(human(4000, 4000)), list(zombie(4000, 4000)));
        assertThat(score, is(0));
    }

    @Test
    public void testWalkToKombo() throws Exception {
        strategy = new ZombieStrategy();

        long score = testScore(player(0, 0), list(human(8000, 8000)), list(zombie(4000, 4000)));
        assertThat(score, is(10));
    }

    private List<Point2D> list(Point2D... points) {
        return new ArrayList<>(Arrays.asList(points));
    }

    private Point2D player(int x, int y) {
        return new Point2D((double) x, (double) y);
    }

    private Point2D zombie(int x, int y) {
        return new Point2D((double) x, (double) y);
    }

    private Point2D human(int x, int y) {
        return new Point2D((double) x, (double) y);
    }

    private long testScore(Point2D player, List<Point2D> humans, List<Point2D> zombies) {
        Simulator simulator = new Simulator(new DataProvider(player.toString(), humans.toString(), zombies.toString(), "test"));
        return simulator.simulate(strategy);
    }

    private static Matcher<Long> is(Integer value) {
        return org.hamcrest.core.Is.is(value.longValue());
    }

    @Test
    public void testDataProviderNotManipulated() throws Exception {
        DataProvider data = DataProvider.hoard();
        Simulator simulator = new Simulator(data, (player, humans, zombies, score) -> {
        });

        simulator.simulate(new HumanStrategy());

        assertThat(data, Matchers.is(DataProvider.hoard()));

    }

    @Test
    public void testCopySimulator() throws Exception {
        Simulator simulator = new Simulator(new DataProvider(player(0, 0).toString(), list(human(0, 0)).toString(), list(zombie(2000, 0), zombie(3500, 0)).toString(), "test"));
        simulator.simulateOneStep(player(3000, 0));
        assertThat(simulator.getCurrentScore(), is(10));

        Simulator simulatorCopy = new Simulator(simulator);
        assertThat(simulatorCopy.getCurrentScore(), is(10));
        simulatorCopy.simulateOneStep(player(3500, 0));
        assertThat(simulator.getCurrentScore(), is(10));
        assertThat(simulatorCopy.getCurrentScore(), is(20));
    }

    @Test
    public void overallScore() throws Exception {
        Long score = 0L;
        for (DataProvider dataProvider : DataProvider.getAllSzenarios()) {
            Simulator simulator = new Simulator(dataProvider);
            score += simulator.highestPossibleScore();
        }
        System.out.println(score);
    }
}
