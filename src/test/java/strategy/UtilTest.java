package strategy;

import data.Point2D;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class UtilTest {

    @Test
    public void testRotation() throws Exception {
        Point2D result = Util.rotateVector(new Point2D(0, 1), 90);
        assertThat(result.getX(), Matchers.closeTo(-1, 0.0001));
        assertThat(result.getY(), Matchers.closeTo(0, 0.0001));
    }

    @Test
    public void testRotation360() throws Exception {
        Point2D result = Util.rotateVector(new Point2D(0, 1), 360);
        assertThat(result.getX(), Matchers.closeTo(0, 0.0001));
        assertThat(result.getY(), Matchers.closeTo(1, 0.0001));
    }

    @Test
    public void testRotation0() throws Exception {
        Point2D result = Util.rotateVector(new Point2D(0, 1), 0);
        assertThat(result.getX(), Matchers.closeTo(0, 0.0001));
        assertThat(result.getY(), Matchers.closeTo(1, 0.0001));
    }
}