package data;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.assertNotNull;

public class DataProviderTest {

    @Test
    public void testParseAll() {
        List<DataProvider> allSzenarios = DataProvider.getAllSzenarios();
        allSzenarios.forEach(szenario -> {
            assertNotNull(szenario.getPlayer());
            assertThat(szenario.getHumans().size(), greaterThanOrEqualTo(1));
            assertThat(szenario.getZombies().size(), greaterThanOrEqualTo(1));
        });
    }

}