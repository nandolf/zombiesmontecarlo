package user_interface;

import data.Point2D;
import org.junit.Test;

import java.util.Arrays;

public class MovesViewTest {


    @Test
    public void test() throws Exception {
        MovesView movesView = new MovesView();
        movesView.setVisible(true);
        movesView.setMoves(Arrays.asList(new Point2D(2, 3),
                new Point2D(2, 3),
                new Point2D(2, 3),
                new Point2D(2, 3),
                new Point2D(2, 3),
                new Point2D(2, 3),
                new Point2D(2, 3),
                new Point2D(2, 3)));
    }
}